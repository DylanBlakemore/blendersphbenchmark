/*
 * cache.cpp
 *
 *  Created on: 04 Jul 2017
 *      Author: dylan
 */
#include "cache.h"
#include <dirent.h>
#include <string>
#include <math.h>
#include <stdlib.h>
#include <algorithm>

int Cache::loadFrame(unsigned int frame_num, CacheFrame* frame_out)
{
	FILE* infile;
	if(frame_num >= file_paths.size()){return 0;}
	std::string spath = file_paths[frame_num];
	infile = fopen(spath.c_str(), "rb");

	if(infile==NULL){
		return 0;
	}

	decodeHeader(frame_out, &infile);

	BlenderParticle p;

	while(decodeNextParticle(&p, &infile)) {
		frame_out->particles.push_back(BlenderParticle(p));
	}

	fclose(infile);
	return 1;
}

int Cache::decodeHeader(CacheFrame* frame, FILE** infile)
{
	int success = 1;
	for(unsigned int i=0; i<8; i++) {
		frame->header.title[i] = fgetc(*infile);
	}
	fread((unsigned int*)(&(frame->header.type)), 4, 1, *infile);
	fread((unsigned int*)(&(frame->header.totpoints)), 4, 1, *infile);
	fread((unsigned int*)(&(frame->header.dtypes)), 4, 1, *infile);

	return success;
}

int Cache::decodeNextParticle(BlenderParticle* p, FILE** infile)
{
	if(feof(*infile))return 0;

	fread((unsigned int*)(&(p->index)), sizeof(p->index), 1, *infile);

	for(int i=0; i<3; i++) {
		fread(&(p->x[i]), sizeof(p->x[i]), 1, *infile);
	}

	for(int i=0; i<3; i++) {
		fread(&(p->v[i]), sizeof(p->v[i]), 1, *infile);
	}

	return 1;
}

int Cache::createFileList()
{
    DIR *dir;
    struct dirent *ent;
    if((dir = opendir(folder.c_str())) != NULL) {
        while((ent = readdir(dir)) != NULL) {
            std::string tmp = folder + ent->d_name;
            int fnum = getFrameNumber(tmp);
            if (fnum == -1)
                continue;
            if(end == 0 && start == 0) {
                file_paths.push_back(tmp);
            }
            else if(fnum >= start && fnum <= end) {
                file_paths.push_back(tmp);
            }
        }
        closedir(dir);
    } else {
        perror("Could not open folder");
        return EXIT_FAILURE;
    }
    std::sort(file_paths.begin(), file_paths.end());
    num_files = file_paths.size();
    return 1;
}

int Cache::getFrameNumber(std::string spath)
{
    int bphys_pos = spath.rfind(".bphys");
    if(bphys_pos == -1)
        return -1;
    int uscore_pos = spath.rfind("_");
    if(uscore_pos == -1)
        return -1;
    std::string sframe = spath.substr(uscore_pos-6, 6);
    return atoi(sframe.c_str());
}



