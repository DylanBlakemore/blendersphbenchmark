/*
 * matrix3d.h
 *
 *  Created on: 08 Sep 2017
 *      Author: dylan
 */

#ifndef MATRIX3D_H_
#define MATRIX3D_H_
#include "cacheframe.h"

struct Bin
{
	Bin()
	{
		parts = vector<BlenderParticle>(0);
	}
	vector<BlenderParticle> parts;
};

class Matrix3d {
public:
	Matrix3d(size_t d1=0, size_t d2=0, size_t d3=0) :
        d1(d1), d2(d2), d3(d3), data(d1*d2*d3,Bin())
    {}

    Bin & operator()(size_t i, size_t j, size_t k) {
        return data[i*d2*d3 + j*d3 + k];
    }

    Bin const & operator()(size_t i, size_t j, size_t k) const {
        return data[i*d2*d3 + j*d3 + k];
    }

    int insert(size_t i, size_t j, size_t k, BlenderParticle in)
    {
    	if(i >= d1 || j >= d2 || k >= d3) return 0;
    	int index = i*d2*d3 + j*d3 + k;
    	(data[index]).parts.push_back(in);
    	return 1;
    }

    bool testIndex(size_t i, size_t j, size_t k)
    {
    	if(i < 0 || j < 0 || k < 0) return false;
    	if(i >= d1 || j >= d2 || k >= d3) return false;

    	return true;
    }

private:
    size_t d1,d2,d3;
    std::vector<Bin> data;
};



#endif /* MATRIX3D_H_ */
