//============================================================================
// Name        : SphBenchmarking.cpp
// Author      : Dylan Blakemore
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <math.h>
#include <fstream>
#include "cache.h"
#include "cacheframe.h"
#include "particle.h"
#include "matrix3d.h"

using namespace std;

float getDist(const float* xi, const float* xj)
{
	return sqrt(pow(xi[0] - xj[0],2) +
			  pow(xi[1] - xj[1],2) +
			  pow(xi[2] - xj[2],2));
}

float getWeight(const float* xi, const float* xj, float h)
{
	float dist = getDist(xi, xj);
	float q = dist/h;
	if(q >= 1) return 0;

	float A = 16.0/(M_PI*pow(h,3));
	float B = 0;

	if(q > 0.5)
		B = 3*pow(q,3) - 3*pow(q,2) + 0.5;
	else
		B = pow(1-q,3);

	return A*B;
}

float calculateDensity(BlenderParticle& bp_in, Matrix3d& mat_in,
					   float m, float h, int bx, int by, int bz)
{
	float density = 0;
	for(int i=-1; i < 2; i++) {
		for(int j=-1; j<2; j++) {
			for(int k=-1; k<2; k++) {
				int bx_t = bx + i;
				int by_t = by + j;
				int bz_t = bz + k;

				if(mat_in.testIndex(bx_t, by_t, bz_t)) {
					for(unsigned int p=0; p<mat_in(bx_t, by_t, bz_t).parts.size(); p++) {
						density += m * getWeight(bp_in.x, mat_in(bx_t, by_t, bz_t).parts[p].x, h);
					}
				}
			}
		}
	}
	return density;
}

int main() {
	const int   N_p   = 10000;
	const int   N_f   = 1000;
	const float m     = 0.04;
	const float rho_0 = 200.0;
	const float c     = sqrt(200.0);
	const float mu    = 2;
	const float h     = 0.05;
	const float dt    = 0.005;

	const float x_b[2] = {0.6, 1.0};
	const float y_b[2] = {-0.25, 0.25};
	const float z_b[2] = {-0.25, 0.25};

	const float center[3] = {1.0,0.0,0.0};
	const float c_rad     = 0.05;
	const float p_size	  = 0.001;

	int n_x = ceil((x_b[1] - x_b[0])/h);
	int n_y = ceil((y_b[1] - y_b[0])/h);
	int n_z = ceil((z_b[1] - z_b[0])/h);

	cout << "Bins:" << n_x << "\t" << n_y << "\t" << n_z << endl;

	string folder = "/home/dylan/Blender/Benchmarking/setup_001/10k/";
	Cache* cache = new Cache(folder);
	CacheFrame* frame;
	Matrix3d bins(n_x, n_y, n_z);
	vector<SphParticle> particles(N_p, SphParticle());
	vector<float> drags(N_f, 0.0);
	vector<float> times(N_f, 0.0);

	/* Loop over each frame. Drag force is calculated at each frame. */
	for(int f=0; f<N_f; f++) {
		times[f] = dt*f;
		frame = new CacheFrame();
		cache->loadFrame(f, frame);
		float drag = 0;
		int n_particles = frame->particles.size();
		/* Add particles to bin matrix for quick searching when calculating
		 * density.
		 */
		for(int p=0; p<n_particles; p++) {
			BlenderParticle bp = frame->particles[p];
			int bin_x = floor((bp.x[0] - x_b[0])/h);
			int bin_y = floor((bp.x[1] - y_b[0])/h);
			int bin_z = floor((bp.x[2] - z_b[0])/h);

			if(bin_x < 0 || bin_y < 0 || bin_z < 0)continue;
			if(bin_x >= n_x || bin_y >= n_y || bin_z >= n_z)continue;
			bins.insert(bin_x, bin_y, bin_z, bp);
		}

		for(int p=0; p<n_particles; p++) {
			BlenderParticle bp = frame->particles[p];
			float dist = sqrt(pow(bp.x[0]-center[0],2) +
							  pow(bp.x[1]-center[1],2));

			int index = bp.index;
			int bin_x = floor((bp.x[0] - x_b[0])/h);
			int bin_y = floor((bp.x[1] - y_b[0])/h);
			int bin_z = floor((bp.x[2] - z_b[0])/h);
			float density = calculateDensity(bp, bins, m, h, bin_x, bin_y, bin_z);
			if(particles[index].is_init) {
				particles[index].update(&bp, density);
			} else {
				particles[index] = SphParticle(index, &bp, center,
											   mu, rho_0, density, c);
			}
			float df = 0;
			if(dist < (c_rad + 2*p_size) && dist > c_rad) {
				particles[index].getDragForceContribution(&df);
				drag += 2*M_PI*c_rad*0.5*df;
			}
		}
		drags[f] = drag;
		frame = NULL;
		delete frame;
	}

	ofstream outfile;
	string outfile_name = folder + "drag_forces.dsv";
	outfile.open(outfile_name.c_str());
	for(unsigned int t=0; t<drags.size(); t++) {
		outfile << times[t] << "\t" << drags[t] << endl;
	}

	delete cache;
	delete frame;

	cout << "Finished" << endl;
	return 0;
}
