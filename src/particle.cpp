/*
 * particle.cpp
 *
 *  Created on: 05 Sep 2017
 *      Author: dylan
 */

#include <math.h>
#include "particle.h"

SphParticle::SphParticle()
{
	id = 0;
	mu = 0;
	rho_next = 0;
	rho = 0;
	rho_0 = 0;
	p  = 0;
	c = 0;

	x_prev 	= vector<float>(3);
	x 		= vector<float>(3);
	x_next	= vector<float>(3);

	v_prev 	= vector<float>(3);
	v 		= vector<float>(3);
	v_next  = vector<float>(3);

	n = vector<float>(3);
	z = vector<float>(3);

	is_init = false;
	can_diff = false;
	n_updates = 0;
}
SphParticle::SphParticle(int ID, const BlenderParticle* bp_in, const float* z_in,
	    				 float mu_in, float rho_0_in, float rho_in, float c_in)
{
	id = ID;
	mu = mu_in;
	rho_next = rho_in;
	rho = 0;
	rho_0 = rho_0_in;
	p  = 0;
	c = c_in;

	x_prev 	= vector<float>(3);
	x 		= vector<float>(3);
	x_next	= vector<float>(3);

	v_prev 	= vector<float>(3);
	v 		= vector<float>(3);
	v_next  = vector<float>(3);

	n = vector<float>(3);
	z = vector<float>(3);

	for(int i=0; i<3; i++) {
		x_next[i] = bp_in->x[i];
		v_next[i] = bp_in->v[i];
		z[i] = z_in[i];
	}

	is_init = true;
	can_diff = false;
	n_updates = 0;
}

SphParticle::~SphParticle()
{
}

void SphParticle::calculateNormal()
{
	n[0] = z[0] - x[0];
	n[1] = z[1] - x[1];
	n[2] = 0;

	// Normalize the normal vector
	float n_mod = sqrt(pow(n[0],2) + pow(n[1],2));
	n[0] = n[0]/n_mod;
	n[1] = n[1]/n_mod;
}

float SphParticle::getSpeedSquared()
{
	return v[0]*v[0] + v[1]*v[1] + v[2]*v[2];
}

void SphParticle::update(const BlenderParticle* bp_in, float rho_in)
{
	n_updates++;
	for(int i=0; i<3; i++) {
		x_prev[i] = x[i];
		v_prev[i] = v[i];

		x[i] = x_next[i];
		v[i] = v_next[i];

		x_next[i] = bp_in->x[i];
		v_next[i] = bp_in->v[i];
	}
	calculateNormal();
	rho = rho_next;
	rho_next = rho_in;
	p = c*c*(rho-rho_0);
}

int SphParticle::getDragForceContribution(float* fd_out)
{
	if(n_updates < 2) {
		*fd_out = 0;
		return 0;
	}
	//std::cout << "rho = " << rho << std::endl;
	if((x[0] - x_prev[0]) == 0 || (x[1] - x_prev[1]) == 0) {
		*fd_out = 0;
		return 1;
	}
	float du1_dx = (v_next[0] - v_prev[0])/(x_next[0] - x_prev[0]);
	float du1_dy = (v_next[0] - v_prev[0])/(x_next[1] - x_prev[1]);

	*fd_out = rho * mu * (du1_dx*n[0] + du1_dy*n[1]) - p*n[0];
	return 1;
}


