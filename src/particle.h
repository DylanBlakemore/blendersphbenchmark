/*
 * particle.h
 *
 *  Created on: 05 Sep 2017
 *      Author: dylan
 */

#ifndef PARTICLE_H_
#define PARTICLE_H_

#include "cacheframe.h"
#include <iostream>

using namespace std;

class SphParticle
{
public:
	SphParticle();
	SphParticle(int ID, const BlenderParticle* bp_in, const float* z_in,
			    float mu_in, float rho_0_in, float rho_in, float c_in);
	~SphParticle();

	int getDragForceContribution(float* fd_out);
	int getLiftForceContribution(float* fl_out);

	void update(const BlenderParticle* bp_in, float rho_in);

	bool is_init;
	bool can_diff;

private:
	void calculateNormal();
	float getSpeedSquared();

	int id;

	vector<float> x_prev;
	vector<float> x;
	vector<float> x_next;

	vector<float> v_prev;
	vector<float> v;
	vector<float> v_next;

	vector<float> n;
	vector<float> z; // center of cylinder

	float p;
	float mu;
	float rho;
	float rho_next;
	float rho_0;
	float c;

	int n_updates;
};



#endif /* PARTICLE_H_ */
