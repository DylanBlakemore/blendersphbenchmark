/*
 * cacheframe.h
 *
 *  Created on: 04 Jul 2017
 *      Author: dylan
 */

#ifndef CACHEFRAME_H_
#define CACHEFRAME_H_

#include <vector>
struct BlenderParticle
{
	unsigned int index;
	float x[3];
	float v[3];
};

struct Header
{
	char title[8];
	unsigned int type;
	unsigned int totpoints;
	unsigned int dtypes;
};

struct CacheFrame
{
	Header header;
	std::vector<BlenderParticle> particles;
};



#endif /* CACHEFRAME_H_ */
